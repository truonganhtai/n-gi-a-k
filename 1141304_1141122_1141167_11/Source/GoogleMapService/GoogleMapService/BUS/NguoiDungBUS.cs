﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GoogleMapService.DAO;
using GoogleMapService.DTO;

namespace GoogleMapService.BUS
{
    public class NguoiDungBUS
    {
        NguoiDungDAO NDDAO = new NguoiDungDAO();
        public NguoiDungDTO LayNguoiDung(int ma)
        {
            return NDDAO.LayNguoiDung(ma);
        }
        public bool ThemNguoiDung(NguoiDung nd)
        {
            return NDDAO.ThemNguoiDung(nd);
        }
        public bool KiemTraDangNhap(string username, string pass)
        {
            return NDDAO.KiemTraDangNhap(username,pass);
        }

        public bool CapNhatViTri(string bienusername, float lat, float lng)
        {
            return NDDAO.CapNhatViTri(bienusername, lat, lng);
        }

        public NguoiDungDTO LayThongTinCaNhan(string bienusername)
        {
            return NDDAO.LayThongTinCaNhan(bienusername);
        }

        public bool SuaNguoiDung(NguoiDung nd)
        {
            return NDDAO.SuaNguoiDung(nd);
        }
        public NguoiDungDTO[] DanhSachBanThan(int ma)
        {
            return NDDAO.DanhSachBanThan(ma);
        }

        public List<NguoiDungDTO> TimViTri(string bk1, string lat1, string lng1)
        {
            return NDDAO.TimViTri(bk1,lat1,lng1);
        }

        public ThongTinVaTuongDAO ThongTinVaTuongCuaNguoiDung(string username)
        {
            return NDDAO.ThongTinVaTuongCuaNguoiDung(username);
        }

        public void GuiTinNhan(QuanLyTuong qlt)
        {
            NDDAO.GuiTinNhan(qlt);
        }

        public bool ThemBan(string usernamecuaban, string usernamenguoikhac)
        {
            return NDDAO.ThemBan(usernamecuaban,usernamenguoikhac);
        }
        public bool CapNhatTuongCuaBan(TuongCuaNguoiDung tuong)
        {
            return NDDAO.CapNhatTuongCuaBan(tuong);
        }
    }
}