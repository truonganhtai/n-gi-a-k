﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using GoogleMapService.BUS;
using GoogleMapService.DTO;

namespace GoogleMapService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    public class NTService : INTService
    {

        public string Tra()
        {
            return "aaa";
        }

        GoogleMapEntities context = new GoogleMapEntities();
        NguoiDungBUS NDBUS = new NguoiDungBUS();
        public NguoiDungDTO LayNguoiDung()
        {
            return NDBUS.LayNguoiDung(1);
        }
        public string ThemNguoiDung(NguoiDung nd)
        {
            bool kq = NDBUS.ThemNguoiDung(nd);
            if (kq == true)
                return "Thanh Cong";
            return "That Bai";
        }

        public bool KiemTraDangNhap(string user, string pass)
        {
            return NDBUS.KiemTraDangNhap(user,pass);
        }

        public bool CapNhatViTri(string bienusername, string lat, string lng)
        {
            return NDBUS.CapNhatViTri(bienusername, float.Parse(lat), float.Parse(lng));
        }

        public string LayThongTinCaNhan(string bienusername)
        {
            NguoiDungDTO temp = NDBUS.LayThongTinCaNhan(bienusername);
            return temp.id + "," + temp.username + "," + temp.password + "," + temp.tennd + "," + temp.diachi + "," + temp.lat + "," + temp.lng;
        }

        public bool SuaNguoiDung(NguoiDung nd) 
        {
            NDBUS.SuaNguoiDung(nd);
            return true;
        }

        public NguoiDungDTO[] DanhSachBanThan(string ma)
        {
            int id = int.Parse(ma);
            return NDBUS.DanhSachBanThan(id);
        }

        public List<NguoiDungDTO> TimViTri(string bk, string lat, string lng)
        {
            return NDBUS.TimViTri(bk,lat,lng);
        }

        public ThongTinVaTuongDAO ThongTinCaNhanVaTuong(string username)
        {
            return NDBUS.ThongTinVaTuongCuaNguoiDung(username);
        }

        public string GuiTinNhan(QuanLyTuong qlt)
        {
            NDBUS.GuiTinNhan(qlt);
            return "true";
        }

        public bool ThemBan(string usernamecuaban, string usernamenguoikhac)
        {
            return NDBUS.ThemBan(usernamecuaban,usernamenguoikhac);
        }
        public bool CapNhatTuongCuaBan(TuongCuaNguoiDung tuong)
        {
            return NDBUS.CapNhatTuongCuaBan(tuong);
        }
    }
}
