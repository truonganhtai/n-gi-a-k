﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using GoogleMapService.DTO;

namespace GoogleMapService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface INTService
    {

        [OperationContract]
        [WebGet(UriTemplate = "gm/lay1", ResponseFormat = WebMessageFormat.Json)]
        string Tra();

        [OperationContract]
        [WebGet(UriTemplate = "gm/lay", ResponseFormat = WebMessageFormat.Json)]
        NguoiDungDTO LayNguoiDung();

        [OperationContract]
        [WebInvoke(UriTemplate = "gm/them", Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string ThemNguoiDung(NguoiDung nd);

        [OperationContract]
        [WebGet(UriTemplate = "gm/dangnhap/{user},{pass}", ResponseFormat = WebMessageFormat.Json)]
        Boolean KiemTraDangNhap(string user, string pass);

        [OperationContract]
        [WebGet(UriTemplate = "gm/capnhatvitri/{bienusername},{lat},{lng}", ResponseFormat = WebMessageFormat.Json)]
        Boolean CapNhatViTri(string bienusername, string lat, string lng);

        [OperationContract]
        [WebGet(UriTemplate = "gm/laythongtincanhan/{tennd}", ResponseFormat = WebMessageFormat.Json)]
        string LayThongTinCaNhan(string tennd);

        [OperationContract]
        [WebInvoke(UriTemplate = "gm/capnhatnguoidung", Method = "PUT", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Boolean SuaNguoiDung(NguoiDung nd);

        [OperationContract]
        [WebGet(UriTemplate = "gm/laydanhsachbanthan/{ma}", ResponseFormat = WebMessageFormat.Json)]
        NguoiDungDTO[] DanhSachBanThan(string ma);

        [OperationContract]
        [WebGet(UriTemplate = "gm/timvitri/{bk},{lat},{lng}", ResponseFormat = WebMessageFormat.Json)]
        List<NguoiDungDTO> TimViTri(string bk, string lat, string lng);

        [OperationContract]
        [WebGet(UriTemplate = "gm/thongtincanhanvatuong/{username}", ResponseFormat = WebMessageFormat.Json)]
        ThongTinVaTuongDAO ThongTinCaNhanVaTuong(string username);

        [OperationContract]
        [WebInvoke(UriTemplate = "gm/guitinnhan", Method = "POST", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string GuiTinNhan(QuanLyTuong qlt);

        [OperationContract]
        [WebGet(UriTemplate = "gm/themban/{usernamecuaban},{usernamenguoikhac}", ResponseFormat = WebMessageFormat.Json)]
        Boolean ThemBan(string usernamecuaban, string usernamenguoikhac);

        [OperationContract]
        [WebInvoke(UriTemplate = "gm/capnhattuongcuaban", Method = "PUT", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Boolean CapNhatTuongCuaBan(TuongCuaNguoiDung tuong);
    }
}
