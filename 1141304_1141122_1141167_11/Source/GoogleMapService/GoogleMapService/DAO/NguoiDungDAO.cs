﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GoogleMapService.DTO;

namespace GoogleMapService.DAO
{
    public class NguoiDungDAO
    {
        GoogleMapEntities context = new GoogleMapEntities();
        public bool KiemTraTonTai(int ma)
        {
            foreach (NguoiDung item in context.NguoiDungs)
            {
                if (item.Id == ma)
                    return true;
            }
            return false;
        }
        public bool KiemTraDangNhap(string username, string pass)
        {
            foreach (NguoiDung item in context.NguoiDungs)
            {
                if (item.UserName == username && item.Pass == pass)
                    return true;
                if (item.UserName.Equals(username) && item.Pass.Equals(pass))
                    return true;
            }
            return false;
        }
        public NguoiDungDTO LayNguoiDung(int ma)
        {
            NguoiDung temp = new NguoiDung();
            if (KiemTraTonTai(ma)==false)
            {
                return null;
            }
            temp = (from sv in context.NguoiDungs
                    where sv.Id == ma
                    select sv).First();
            NguoiDungDTO nd = new NguoiDungDTO();
            nd.id = temp.Id;
            nd.username = temp.UserName;
            nd.password = temp.Pass;
            nd.tennd = temp.TenND;
            nd.diachi = temp.DiaChi;
            if (temp.Lat.ToString()!="")
                nd.lat = float.Parse(temp.Lat.ToString());
            if (temp.Lng.ToString() != "")
                nd.lng = float.Parse(temp.Lng.ToString());
            return nd;
        }

        public bool ThemNguoiDung(NguoiDung nd) 
        {
            foreach (NguoiDung item in context.NguoiDungs)
            {
                if (item.UserName.Equals(nd.UserName))
                    return false;
            }
            context.NguoiDungs.AddObject(nd);
            context.SaveChanges();
            return true;
        }

        public bool CapNhatViTri(string bienusername, float lat, float lng)
        {
            NguoiDung temp = (from nd in context.NguoiDungs
                              where nd.UserName == bienusername
                            select nd).First();
            temp.Lat = lat;
            temp.Lng = lng;
            context.SaveChanges();
            return true;
        }

        public NguoiDungDTO LayThongTinCaNhan(string bienusername)
        {
            NguoiDung temp = new NguoiDung();
            temp = (from sv in context.NguoiDungs
                    where sv.UserName == bienusername
                    select sv).First();
            NguoiDungDTO nd = new NguoiDungDTO();
            nd.id = temp.Id;
            nd.username = temp.UserName;
            nd.password = temp.Pass;
            nd.tennd = temp.TenND;
            nd.diachi = temp.DiaChi;
            if (temp.Lat.ToString() != "")
                nd.lat = float.Parse(temp.Lat.ToString());
            if (temp.Lng.ToString() != "")
                nd.lng = float.Parse(temp.Lng.ToString());
            return nd;
        }

        public bool SuaNguoiDung(NguoiDung nd)
        {
            NguoiDung temp = new NguoiDung();
            temp = (from sv in context.NguoiDungs
                    where sv.Id == nd.Id
                    select sv).First();
            temp.UserName = nd.UserName;
            temp.Pass = nd.Pass;
            temp.TenND = nd.TenND;
            temp.DiaChi = nd.DiaChi;
            temp.Lat = nd.Lat;
            temp.Lng = nd.Lng;
            context.SaveChanges();
            return true;
        }

        public NguoiDungDTO[] DanhSachBanThan(int ma)
        {
            NguoiDungDTO []a = new NguoiDungDTO[context.NguoiDungs.Count()];
            int i = 0;
            foreach (DanhSachBan item1 in context.DanhSachBans)
            {
                if(item1.IdND1 == ma)
                {
                    foreach (NguoiDung item in context.NguoiDungs)
                    {
                        if(item.Id == item1.IdND2)
                        {
                            NguoiDungDTO nd = new NguoiDungDTO();
                            nd.id = item.Id;
                            nd.username = item.UserName;
                            nd.password = item.Pass;
                            nd.tennd = item.TenND;
                            nd.diachi = item.DiaChi;
                            nd.lat = float.Parse(item.Lat.ToString());
                            nd.lng = float.Parse(item.Lng.ToString());
                            a[i] = nd;
                            i++;
                        }
                    }
                }
            }
            
            return a;
        }

        public double KhoangCach(float lat1, float lng1, float lat2, float lng2)
        {
            float R = 6371;
            double dLat = Math.PI * (lat2 - lat1) / 180.0;
            double dLon = Math.PI * (lng2 - lng1) / 180.0;
            double dlat1 = Math.PI * lat1 / 180.0;
            double dlat2 = Math.PI * lat2 / 180.0;
            double a = Math.Sin(dLat / 2) * Math.Sin(dLat / 2) + Math.Cos(dlat1) * Math.Cos(dlat2) * Math.Sin(dLon / 2) * Math.Sin(dLon / 2);
            double c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
            double kc = R * c;
            return kc;
        }

        public List<NguoiDungDTO> TimViTri(string bk1, string lat1, string lng1)
        {
            string[] a = lat1.Split('-');
            string lat = a[0] + "." + a[1];

            string[] b = lng1.Split('-');
            string lng = b[0] + "." + b[1];
            bool kt = false;
            string[] c = bk1.Split('-');
            string bk2 = "";
            int dem = c.Length;
            int k = c.Count();
            if (dem > 1)
            {
                kt = true;
                bk2 = c[0] + "." + c[1];
            }
            if (kt == false)
                bk2 = bk1;
            double bk = double.Parse(bk2.ToString());

            //double bk = Math.PI * bk3 / 180.0;
            List<NguoiDungDTO> ndr = new List<NguoiDungDTO>();
            foreach (NguoiDung item in context.NguoiDungs)
            {
                double kc = KhoangCach(float.Parse(lat), float.Parse(lng), float.Parse(item.Lat.ToString()), float.Parse(item.Lng.ToString()));
                if (bk >= kc)
                {
                    NguoiDungDTO nd = new NguoiDungDTO();
                    nd.id = item.Id;
                    nd.username = item.UserName;
                    nd.password = item.Pass;
                    nd.tennd = item.TenND;
                    nd.diachi = item.DiaChi;
                    nd.lat = float.Parse(item.Lat.ToString());
                    nd.lng = float.Parse(item.Lng.ToString());
                    ndr.Add(nd);
                }
            }
            return ndr;
        }

        public ThongTinVaTuongDAO ThongTinVaTuongCuaNguoiDung(string username)
        {
            ThongTinVaTuongDAO temp = new ThongTinVaTuongDAO();
            foreach(TuongCuaNguoiDung item in context.TuongCuaNguoiDungs)
            {
                if (item.username.Equals(username))
                {
                    NguoiDung nd = new NguoiDung();
                    nd = (from sv in context.NguoiDungs
                            where sv.UserName == username
                            select sv).First();
                    temp.id = 1;
                    temp.tuongcuaban = item.tuongcuaban;
                    temp.username = username;
                    temp.diachi = nd.DiaChi;
                    temp.lat = float.Parse(nd.Lat.ToString());
                    temp.lng =  float.Parse(nd.Lng.ToString());
                    return temp;
                }
            }
            return null;
        }

        public void GuiTinNhan(QuanLyTuong qlt)
        {
            context.QuanLyTuongs.AddObject(qlt);
            context.SaveChanges();
        }

        public bool ThemBan(string usernamecuaban, string usernamenguoikhac)
        {
            NguoiDung ndlaban = new NguoiDung();
            ndlaban = (from sv in context.NguoiDungs
                       where sv.UserName == usernamecuaban
                  select sv).First();
             NguoiDung nd = new NguoiDung();
             int flag = 0;
             foreach (NguoiDung item in context.NguoiDungs)
             {
                 if (item.UserName.Equals(usernamenguoikhac))
                 {
                     nd = item;
                     flag = 1;
                 }
             }
             foreach (DanhSachBan item in context.DanhSachBans)
             {
                 if (item.IdND1 == ndlaban.Id && item.IdND2 == nd.Id)
                     return false;
                 if (item.IdND2 == ndlaban.Id && item.IdND1 == nd.Id)
                     return false;
             }
             if (flag==1)
             {
                 DanhSachBan dsb = new DanhSachBan();
                 dsb.IdND1 = ndlaban.Id;
                 dsb.IdND2 = nd.Id;
                 context.DanhSachBans.AddObject(dsb);
                 context.SaveChanges();
                 return true;
             }
            return false;
        }

        public bool CapNhatTuongCuaBan(TuongCuaNguoiDung tuong)
        {
            int flag = 0;
            TuongCuaNguoiDung temp = new TuongCuaNguoiDung();
            foreach (TuongCuaNguoiDung item in context.TuongCuaNguoiDungs)
            {
                if (item.username.Equals(tuong.username))
                {
                    flag = 1;
                    temp = item;
                }
            }
            if (flag == 0)
            {
                context.TuongCuaNguoiDungs.AddObject(tuong);
                context.SaveChanges();
            }
            if (flag == 1)
            {
                temp.tuongcuaban = tuong.tuongcuaban;
                context.SaveChanges();
            }
            return true;
        }
    }
}