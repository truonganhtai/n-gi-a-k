USE [GoogleMap]
GO
/****** Object:  Table [dbo].[TuongCuaNguoiDung]    Script Date: 12/28/2012 13:25:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TuongCuaNguoiDung](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[username] [nvarchar](100) NULL,
	[tuongcuaban] [nvarchar](max) NULL,
 CONSTRAINT [PK_TuongCuaNguoiDung] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[TuongCuaNguoiDung] ON
INSERT [dbo].[TuongCuaNguoiDung] ([id], [username], [tuongcuaban]) VALUES (1, N'admin', N'hôm nay :(!!!')
INSERT [dbo].[TuongCuaNguoiDung] ([id], [username], [tuongcuaban]) VALUES (2, N'lethu', N'hôm nay vui không?')
INSERT [dbo].[TuongCuaNguoiDung] ([id], [username], [tuongcuaban]) VALUES (3, N'vochientruong', N'hôm nay cũng vui vui!!!')
SET IDENTITY_INSERT [dbo].[TuongCuaNguoiDung] OFF
/****** Object:  Table [dbo].[QuanLyTuong]    Script Date: 12/28/2012 13:25:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QuanLyTuong](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[usernamenguoigui] [nvarchar](100) NULL,
	[usernamenguoinhan] [nvarchar](100) NULL,
	[noidung] [nvarchar](max) NULL,
 CONSTRAINT [PK_QuanLyTuong] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[QuanLyTuong] ON
INSERT [dbo].[QuanLyTuong] ([id], [usernamenguoigui], [usernamenguoinhan], [noidung]) VALUES (1, N'lethu', N'hauan', N'mai đi học không?')
INSERT [dbo].[QuanLyTuong] ([id], [usernamenguoigui], [usernamenguoinhan], [noidung]) VALUES (2, N'admin', N'lethu', N'hello lệ thu, mai đi học không dậy?')
INSERT [dbo].[QuanLyTuong] ([id], [usernamenguoigui], [usernamenguoinhan], [noidung]) VALUES (3, N'admin', N'hauan', N'hôm nay vui không?')
INSERT [dbo].[QuanLyTuong] ([id], [usernamenguoigui], [usernamenguoinhan], [noidung]) VALUES (4, N'vochientruong', N'NguyenD', N'hôm nay đi học không?')
SET IDENTITY_INSERT [dbo].[QuanLyTuong] OFF
/****** Object:  Table [dbo].[NguoiDung]    Script Date: 12/28/2012 13:25:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NguoiDung](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](100) NULL,
	[Pass] [nvarchar](100) NULL,
	[TenND] [nvarchar](100) NULL,
	[DiaChi] [nvarchar](100) NULL,
	[Lat] [float] NULL,
	[Lng] [float] NULL,
 CONSTRAINT [PK_NguoiDung] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[NguoiDung] ON
INSERT [dbo].[NguoiDung] ([Id], [UserName], [Pass], [TenND], [DiaChi], [Lat], [Lng]) VALUES (1, N'admin', N'admin', N'Thằng Admin', N'235B/A11/13 Nguyễn Văn Cừ', 10.206813, 106.364136)
INSERT [dbo].[NguoiDung] ([Id], [UserName], [Pass], [TenND], [DiaChi], [Lat], [Lng]) VALUES (2, N'chientruong', N'chientruong', N'Võ Chiến Trường', N'235S/C11/15 Nguyễn Văn Cừ', 10.75918, 106.662498)
INSERT [dbo].[NguoiDung] ([Id], [UserName], [Pass], [TenND], [DiaChi], [Lat], [Lng]) VALUES (3, N'1', N'1', N'1', N'1', 10.217625, 106.358642)
INSERT [dbo].[NguoiDung] ([Id], [UserName], [Pass], [TenND], [DiaChi], [Lat], [Lng]) VALUES (4, N'hauan', N'123456', N'Võ Hậu An', N'Quảng Ngãi', 11.845847, 108.457031)
INSERT [dbo].[NguoiDung] ([Id], [UserName], [Pass], [TenND], [DiaChi], [Lat], [Lng]) VALUES (5, N'lethu', N'123456', N'Võ Thị Lệ Thu', N'Quảng Ngãi', 10.395975, 107.138672)
INSERT [dbo].[NguoiDung] ([Id], [UserName], [Pass], [TenND], [DiaChi], [Lat], [Lng]) VALUES (6, N'hoangnam', N'123456', N'Hoàng Nam', N'Vũng Tàu', 10.75918, 106.662498)
INSERT [dbo].[NguoiDung] ([Id], [UserName], [Pass], [TenND], [DiaChi], [Lat], [Lng]) VALUES (7, N'NguyenA', N'123456', N'Nguyễn Văn A', N'Hà Nội', 20.899871, 105.88623)
INSERT [dbo].[NguoiDung] ([Id], [UserName], [Pass], [TenND], [DiaChi], [Lat], [Lng]) VALUES (8, N'NguyenB', N'123456', N'Nguyễn Văn B', N'Cao Bằng', 22.745789, 106.270752)
INSERT [dbo].[NguoiDung] ([Id], [UserName], [Pass], [TenND], [DiaChi], [Lat], [Lng]) VALUES (9, N'NguyenC', N'123456', N'Nguyễn Văn C', N'Quảng Bình', 17.329665, 106.6333)
INSERT [dbo].[NguoiDung] ([Id], [UserName], [Pass], [TenND], [DiaChi], [Lat], [Lng]) VALUES (10, N'NguyenD', N'123456', N'Nguyễn Văn D', N'Bình Định', 13.771399, 109.173889)
INSERT [dbo].[NguoiDung] ([Id], [UserName], [Pass], [TenND], [DiaChi], [Lat], [Lng]) VALUES (11, N'NguyenE', N'123456', N'Nguyễn Văn E', N'Lào', 15.432501, 106.21582)
INSERT [dbo].[NguoiDung] ([Id], [UserName], [Pass], [TenND], [DiaChi], [Lat], [Lng]) VALUES (12, N'', N'123456', N'', N'', 10.75918, 106.662498)
INSERT [dbo].[NguoiDung] ([Id], [UserName], [Pass], [TenND], [DiaChi], [Lat], [Lng]) VALUES (13, N'NguyenF', N'123456', N'Nguyễn Anh F', N'Thái Lan', 10.271681, 106.33667)
INSERT [dbo].[NguoiDung] ([Id], [UserName], [Pass], [TenND], [DiaChi], [Lat], [Lng]) VALUES (14, N'vochientruong', N'123456', N'võ chiến trường', N'xóm c', 15.014359, 108.424072)
SET IDENTITY_INSERT [dbo].[NguoiDung] OFF
/****** Object:  Table [dbo].[DanhSachBan]    Script Date: 12/28/2012 13:25:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DanhSachBan](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[IdND1] [int] NULL,
	[IdND2] [int] NULL,
 CONSTRAINT [PK_DanhSachBan] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[DanhSachBan] ON
INSERT [dbo].[DanhSachBan] ([id], [IdND1], [IdND2]) VALUES (1, 1, 2)
INSERT [dbo].[DanhSachBan] ([id], [IdND1], [IdND2]) VALUES (2, 2, 1)
INSERT [dbo].[DanhSachBan] ([id], [IdND1], [IdND2]) VALUES (3, 1, 3)
INSERT [dbo].[DanhSachBan] ([id], [IdND1], [IdND2]) VALUES (4, 1, 4)
INSERT [dbo].[DanhSachBan] ([id], [IdND1], [IdND2]) VALUES (5, 1, 5)
INSERT [dbo].[DanhSachBan] ([id], [IdND1], [IdND2]) VALUES (6, 5, 4)
INSERT [dbo].[DanhSachBan] ([id], [IdND1], [IdND2]) VALUES (7, 5, 3)
INSERT [dbo].[DanhSachBan] ([id], [IdND1], [IdND2]) VALUES (8, 4, 4)
INSERT [dbo].[DanhSachBan] ([id], [IdND1], [IdND2]) VALUES (9, 1, 7)
INSERT [dbo].[DanhSachBan] ([id], [IdND1], [IdND2]) VALUES (10, 1, 8)
INSERT [dbo].[DanhSachBan] ([id], [IdND1], [IdND2]) VALUES (11, 14, 1)
INSERT [dbo].[DanhSachBan] ([id], [IdND1], [IdND2]) VALUES (12, 14, 7)
INSERT [dbo].[DanhSachBan] ([id], [IdND1], [IdND2]) VALUES (13, 14, 8)
INSERT [dbo].[DanhSachBan] ([id], [IdND1], [IdND2]) VALUES (14, 14, 9)
INSERT [dbo].[DanhSachBan] ([id], [IdND1], [IdND2]) VALUES (15, 14, 10)
INSERT [dbo].[DanhSachBan] ([id], [IdND1], [IdND2]) VALUES (16, 14, 11)
SET IDENTITY_INSERT [dbo].[DanhSachBan] OFF
