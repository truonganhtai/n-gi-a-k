﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoogleMapService.DTO
{
    public class NguoiDungDTO
    {
        public int id { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string tennd { get; set; }
        public string diachi { get; set; }
        public float lat { get; set; }
        public float lng { get; set; }
    }
}